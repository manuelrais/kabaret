# Changelog

## [2.1.4] - 2019-08-09

### Added

#12 - Dynamic UI Properties

Any property set on a `Relation.ui()` can now be overridden per Object instance by overridding `Object._fill_ui(ui)`
This has been a long request, especially to have dynamic value for the `icon` and `hidden` properties.

#48 - Custom UI Page

It is now possible to provide an arbitrary widget to display/edit an Object in the Flow view.

This can be specified at the relation's level with `Relation(...).ui(custom_page='my_module.MyWidget')`.
Or at the Object level by overridding `Object._fill_ui(ui)`.

See `dev_studio.flow.unittest_project.UnittestProject` "Custom Page" for details and example.

### Fixed

issue #12 - When expanding a Connection relation in GUI, sub fields don't update.
issue #12 - (correlated) When showing not-child item in DynamicMap (a.k.a Map as View), items don't update.

issue #47 - Fix Map item styling consistency

The `style` dict in `Map._fill_row_style` now accepts 'foreground_color' as well as 'foreground-color' etc... And
the value may be in the form (255,255,255) as well as '#FFFFFF'

## [2.1.3] - 2019-07-15

### Fixed

issue #43 - MapField summary refresh after touch().

issue #44 - Maya lose QMenu reference when using QMenu.addMenu.

### Added

Support per row as well as per column color properties in Map row style (see !73)

## [2.1.2] - 2019-04-09

### Changed

Documentation minor improvements: issue #36 #38 #39 #40

### Fixed

issue #24 - Ctrl+Click on map item works only once.
issue #37 - dev_studio UnittestProject wouldn't work without kabaret.ingrid installed.

## [2.1.1] - 2019-03-05

### Changed

Dropped the beta status because why not.

### Added

#29 - Added context based GUI filtering for flow.Action
+ updated dev_studio.unittest_project.showcase with doc and examples.

## [2.1.0b3] - 2019-02-05

### Fixed

python 3 compatibility

## [2.1.0b1] - 2019-01-18

### Changed

issue #25 - Refactor View class hierarchy to be able to have Toolbar and Dialog views
issue #26 - View management RFE
issue #13 - Add an option in flow.Action.get_result() to force a "goto" in a new view

- Added deprecation warnings (stdout printed) for view system changes. You should update
your code as soon as possible.
Once your code is updated, you should also update your install_requires with 'kabaret>=2.1'

- Added a built-in 'SessionToolBar' view with user/session/cluster info and layout preset tools.
The layout preset tools won't be useful without the upcoming 'Users' actor. Once this actor is
built-in, the SessionToolBar will be added to the default Standalone GUI Session.

### Fixed

issue #23 - Dialog inside another Dialog

## [2.0.0b17] - 2019-01-07

### Changed

install_requires now specify redis version <3.0.0

## [2.0.0b15] - 2018-12-17

### Fixed

issue #2: support for file drop in PythonValueEditor

## [2.0.0b15] - 2018-12-11

### Fixed

Removed Action buttons in FlowPage.

## [2.0.0b14] - 2018-11-16

### Fixed

issue #22: Error in action dialog with sub-fields

## [2.0.0b13] - 2018-11-15

### Fixed

issue #21: Potential event buffer over-growth in Cluster Actor

### Added

new icons.gui resource: kabaret_logo_vector.svg

## [2.0.0b12] - 2018-10-22

### Fixed

issue #10: Project reload related

## [2.0.0b11] - 2018-10-18

### Fixed

issue #14: Unable to watch a Connection related Ref

issue #15: Map.add may sometimes mess up the ValueStore

issue #9: Map.add(name) doesn't raise if the name is already used

Label editor not showing error

Fail to log exception raised in session.tick() dispatch

## [2.0.0b10] - 2018-10-02

### Fixed
- Flow actor related bugs

## [2.0.0b8] - 2018-09-30

### Changed
- Flow actor's Home can now be overriden by a custom one.

## [2.0.0b7] - 2018-09-27

### Added
- Changelog

### Fixed
- Wrong python requires preventing pip install on python 2.7
