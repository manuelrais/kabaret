.. Kabaret documentation master file, created by
   sphinx-quickstart on Fri Jan  5 17:58:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kabaret's documentation
==================================

**Kabaret** is a Free and Open Source VFX/Animation Studio Framework.

It is made for TDs and Scripters involved in Production Tracking, Asset Management, Workflow and Pipelines used by Production Managers and CG Artists.

Main Features
-------------

* Fast and Easy Project modeling.
* No decision made for you, your pipe = your rules !
* Based on 20+ years of `experience <credits.html>`_ in the field.
* Generative end-user GUI: zero code *needed*.
* Modular and Extendable pure-python architecture.
* Python 2.7+ and 3.6+ compatible.
* Embeddable in PyQt5, PyQt4 and PySide based applications.
* Tested under Windows and Linux.
* There's an example project so you can start experimenting in less than 5 minutes !

Status
------
**Kabaret** has been used in productions for over two years by a couple of studios, with only minor changes made in the second year.
This has been the signal that it was ready to be shared with the world.

We were wrong :p

Being bug-free and optimally tweaked did not shade the fact that the original specs were containing features
that had never been used during those two years.
So we decided to refactor in order to drop the non-vital parts that were adding unnecessary complexity (namely: the station/client architecture, the filesystem database, some Flow features, most Actors...).

This refactoring is done, but we lost compatibility with previous projects and unit tests. 
One could say we also lost the confidence gained with the two years in productions.
Until unit test are back, there is no arguing against this.

**tl;dr:** Have fun with kabaret, but do not use it in production *yet*.


.. toctree::
   :caption: Introduction
   :maxdepth: 2

   why_and_how.rst

.. .. toctree::
..    :caption: Quick Tour
..    :maxdepth: 2

..    gui_quick_tour.rst
..    flow_quick_tour.rst
..    extending_quick_tour.rst
..    featured_extensions.rst

.. toctree::
   :caption: Tutorials
   :maxdepth: 2

   tutorials/tutorials.rst

.. toctree::
   :caption: Documentations
   :maxdepth: 2

   install.rst
   user_guide.rst
   flow_reference_guide.rst
   app_reference_guide.rst
   featured_extensions.rst

.. toctree::
   :caption: More
   :maxdepth: 2
   
   faq.rst
   credits.rst
   
Indices
=======
* :ref:`genindex`
* :ref:`modindex`
